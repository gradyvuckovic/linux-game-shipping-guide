---
title: "Low Sales Volume"
category: Common Concerns
order: 2
---

**"The number of Linux gamers is a tiny fraction of the overall market, so I
won't receive a high enough number of sales to warrant bothering to release a
native Linux port."**

Ultimately the decision is for you, the developer, to make, to decide if it is
or isn't worth the investment to officially support Linux with a native
application. No one will deny that the overall number of Linux gamers compared
to Windows gamers is significantly lower.

According to [Valve's Steam Hardware Survey,][1] at the time of writing, there
are an estimated 2,400,000 monthly active users who use Linux as their primary
desktop OS on Steam. This number is calculated by taking the 1.82% userbase
statistic from the August 2023 survey, and applying that % to the estimated 132
million 'monthly active users' that [Valve reports][2] to have.

Linux gamers exert incredibly strong brand loyalty to their platform, are very
pro-sumer in their purchases, and often want to actively support developers who
support their platform. *(Which you should take advantage of to increase sales
and gain exposure for your game, see [Leverage Linux Loyalty][3])*

By its nature, the Linux gaming market also has much less competition and
saturation than the Windows gaming market, even on Steam, due to the overall
lower number of available games.

You may find your title will be especially popular if your game is of a genre
that isn't commonly available on Linux. AAA games and multiplayer PvP games are
often not available on Linux, either natively or via Proton *(due to their
frequent use of AntiCheat technology that isn't compatible with Proton)*. If
either of those descriptions fit your game, you should consider that your title
would not be facing much competition on Linux.

Combined with the other [benefits of supporting Linux][4], a properly managed
Linux native port of your game can be very profitable.

[1]: https://store.steampowered.com/hwsurvey
[2]: https://steamcommunity.com/groups/steamworks/announcements/detail/2961646623386540827
[3]: /linux-game-shipping-guide/2-general-advice/leverage-linux-loyalty
[4]: /linux-game-shipping-guide/0-basics/benefits-of-linux-support
