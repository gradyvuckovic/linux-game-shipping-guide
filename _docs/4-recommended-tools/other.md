---
title: "General"
category: "Recommended Tools"
order: 6
---

## Git

Git is the industry standard tool for version control of source code and almost
unavoidable for game developers in general on any OS, including Linux.

The good news is, many Linux distributions come with Git preinstalled out of the
box, and the rest usually can install Git via a single terminal command.

Check out the official [Git website][1] for more information for your Linux
distribution.

If you are unfamiliar Git, check out this video
["What is Git? Explained in 2 Minutes!"][2] by Programming with Mosh for a short
explanation to get started.

## GCC

GCC stands for 'the GNU Compiler Collection', and consists of compilers for C,
C++, Objective-C, Fortran and more. GCC is commonly used on Linux for compiling
C / C++ code projects, including games.

With GCC installed on your PC, you can compile C code projects with `gcc`, and
compile C++ projects with `g++`.

For information on how to use GCC for compiling your C or C++ games, check out
[this short guide][3] by Boston University Department of Computer Science

Additionally check out this short [tutorial video][4] from the Youtube channel
ProgrammingKnowledge2 that provides a simple short demo of how to easily compile
a C++ Hello World application with G++.

## Makefiles

For C/C++ developers, Makefiles will be a common sight on Linux and are used
to automate building applications on Linux, or compiling and installing
dependencies.

For more information on how to use Makefiles or how they work, check out this
great [tutorial][5] by [makefiletutorial.com](https://makefiletutorial.com/).

## CMake

CMake is cross-platform free and open-source software for build automation,
testing, packaging and installation of software by using a
compiler-independent method.

Using a `CMakeLists.txt` file in a project, CMake allows defining the structure
of a project and the compiler targets and options, then generating projects and
builds for different IDEs and compilers.

CMake is a very useful tool for cross platform game developers, more information
and a guide can be found on the [official website for CMake.][6]

A [useful and informative talk](https://www.youtube.com/watch?v=bsXLMQ6WgIk)
titled "Effective CMake" by Daniel Pfeifer is also available on the CppNow
Youtube channel.

## Docker

Docker is a software platform that allows for creating self-contained
containers of software that bring with them all of their own libraries,
configuration files, and which communicate via defined channels. Docker
containers can be thought of as 'lightweight virtual machines'. Similar to how a
virtual machine virtualizes (removes the need to directly manage) server
hardware, containers virtualize the operating system of a server. With simple
commands, you can build, start, and stop containers.

Build once, run everywhere. Even on different operating systems.

Docker can be used to build containers for game servers, allowing you to, for
example, develop and test on a local Windows machine and then deploy those same
containers to a Linux VPS.

Docker can also be used to build containers that contain your game engine, and
used to compile your game for any OS, from any OS. Using this, it is possible to
setup a continuous integration pipeline, where your game is rebuilt with every
'git push' to your repository, and report any failure to compile.

Check out this great video tutorial by Fireship on Docker titled
["Learn Docker in 7 Easy Steps - Full Beginners Tutorial"][7] on Youtube.

[1]: https://git-scm.com/
[2]: https://www.youtube.com/watch?v=2ReR1YJrNOM
[3]: https://www.cs.bu.edu/fac/gkollios/cs113/Usingg++.html
[4]: https://www.youtube.com/watch?v=yXMb7SC9gHg
[5]: https://makefiletutorial.com/
[6]: https://cmake.org/
[7]: https://www.youtube.com/watch?v=gAkwW2tuIqE
