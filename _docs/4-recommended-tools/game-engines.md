---
title: "Game Engines"
category: "Recommended Tools"
order: 3
---

## Godot

Godot is an MIT licensed open source game engine that can be used for free
without any licensing costs. The Godot engine and Godot Editor both support
Linux, and Godot also offers a headless server version of Godot to run on Linux
for multiplayer games. Godot is the perfect choice of engine for 2D games, with
3D support as well, and offers easy 1-click one export to Windows, Mac, Linux,
Android and iOS, with commercial porting services available for supporting game
consoles as well. If your game is developed in Godot, supporting Linux should be
relatively easy.

The Steam Deck is a popular Linux OS based PC for gaming. [A guide for targeting the Steam Deck][2]
with native Linux builds is available on [www.kodeco.com](https://www.kodeco.com).
The guide covers the process from start to finish, establishing a test project
in Godot, setting up builds and transferring a game to the Steam Deck to test.
While the guide specifically refers to the Steam Deck, most of the guide is
applicable to targeting Linux in general for Godot engine users.

## Unity

Unity features Linux support and can compile native Linux games. Recommended
versions of Unity to use are 2017.2 or 2019.x, and to avoid 2017.3 and 2018.3.
If you are using either version it is recommended to upgrade to 2019.x to
[avoid a Vulkan crash on Linux.](https://issuetracker.unity3d.com/issues/vulkan-build-crashes-on-launch-for-linux)

In 2019, Unity announced they will be
[officially supporting Linux for their editor as well,](https://blogs.unity3d.com/2015/08/26/unity-comes-to-linux-experimental-build-now-available/)
due to increased demand.

## Unreal Engine

Unreal Engine has the ability to compile to native Linux and the Unreal Engine
Editor is available for Linux as well. The Unreal Engine Editor requires an
account on the [Unreal Engine website](https://www.unrealengine.com/en-US/).
After signing up, you will need to follow [these instructions](https://www.unrealengine.com/en-US/ue4-on-github)
to access the [official Epic repositories](https://github.com/EpicGames/Signup),
and to access the [Unreal Engine repository](https://github.com/EpicGames/UnrealEngine).
After following these steps, you can clone the git repository of Unreal Engine
and compile it from source.

A [video tutorial][1] by Ben Tristem
is also available to demonstrate how to get started.

[1]: https://www.youtube.com/watch?v=7Zo9DNeVgxE
[2]: https://www.kodeco.com/41495624-targeting-the-steam-deck-with-godot
