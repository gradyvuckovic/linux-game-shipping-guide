---
title: "Recommended Videos"
category: "General Advice"
order: 8
---
A collection of recommended viewing material for game developers new to Linux,
Linux as a gaming platform, or game development on Linux:

#### [Why so many distros? The weird history of Linux][1]
_by Fireship (March 2021)_

A recommended primer on Linux distributions for anyone new to Linux.

#### [Linux gaming is BETTER than Windows?][2]
_by Linus Tech Tips (June 2020)_

Linus and Anthony from Linus Tech Tips take a look at gaming on Linux, and talk
about it's pros and cons objectively and discover it fairs pretty well against
Windows in 2020.

#### [Vulkan and LunarG Explained...][3]
_by Gamesfromscratch (June 2018)_

Recommended viewing for any C++ developer looking at porting their DirectX game
to Vulkan, Mike from Gamesfromscratch explains the LunarG SDK and Vulkan API.

#### [Steamworks Quick Tips - Steam Deck][4]
_by Steamworks Development (July 2021)_

Official video from Valve with advice for game developers interested in
officially supporting the Steam Deck, a new handheld gaming PC from Valve, which
will run Steam OS 3.0, an Arch based Linux distribution.

#### [C++Now 2017: Daniel Pfeifer "Effective CMake"][5]
_by CppNow (June 2017)_

A talk explaining how best to use CMake for cross platform C++ projects. CMake
is an industry standard tool for managing cross platform C++ development, useful
for game developers looking to manage their cross platform game development.

#### [Building Unreal Engine on Linux][6]
_by Ben Tristem (March 2019)_

A quick video which explains everything you need to know to get started using
the Unreal Engine editor on Linux.

#### [Learn Docker in 7 Easy Steps - Full Beginners Tutorial][7]
_by Fireship (August 2020)_

A tutorial to get started with Docker, a useful tool for cross platform server
deployment.

[1]: https://www.youtube.com/watch?v=ShcR4Zfc6Dw
[2]: https://www.youtube.com/watch?v=6T_-HMkgxt0
[3]: https://www.youtube.com/watch?v=wWYRFwIHdJc
[4]: https://www.youtube.com/watch?v=5Q_C5KVJbUw
[5]: https://www.youtube.com/watch?v=bsXLMQ6WgIk
[6]: https://www.youtube.com/watch?v=7Zo9DNeVgxE
[7]: https://www.youtube.com/watch?v=gAkwW2tuIqE
