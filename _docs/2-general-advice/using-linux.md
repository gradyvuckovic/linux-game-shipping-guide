---
title: "Using Linux"
category: "General Advice"
order: 1
---
## Getting Started

Begin by installing a Linux based desktop OS on a PC by following these 7 easy
steps:

 1. Choose a Linux distribution.
 2. Navigate to the distribution's website.
 3. Download the distribution's ISO file.
 4. Write the ISO to a USB drive.
 5. Ensure PC's BIOS is configured to boot from a USB drive.
 6. Insert the USB drive and (re)boot the device.
 7. Follow the on screen prompts and installation process.

Alternatively you can try out a Linux distribution with VM software such as
[Virtual Box][1].

## Choosing a Linux Distribution

Which Linux distribution is for you, is a matter of personal taste. There are
many to choose from.

Such as Debian or Arch or their derivatives, Ubuntu (based on Debian),
Pop!_OS (based on Ubuntu), Manjaro (based on Arch), etc.

If you are new to Linux, it's recommended to try more than one distribution. As
each one provides a different user experience.

For the purposes of game development and game testing, it may be best to choose
a distribution that more closely matches what your customers may be using to
play your games.

In that regard;

 * Google Stadia is reported to use Debian for it's servers.
 * Ubuntu is reported via Steam Stats to be the most commonly used distribution
 by Linux gamers.
 * The Steam Deck by Valve will ship with Steam OS 3.0, which is reportedly a
 fork of Arch with the KDE Plasma desktop environment below the Steam UI layer.
 * Pop!_OS is the default option of preinstalled OS on  PCs sold by
 [System76.][2]
 * Solus, Manjaro and Linux Mint are popular among gamers.

## The Terminal

The Terminal on Linux is equivalent of 'Command Prompt' on Windows.

As a developer on Linux, the terminal will become an essential tool.

Howtogeek provide an excellent [tutorial][3] on the Linux terminal for
beginners that covers the basics to get you started.

## The Linux File System

For developers new to Linux, the Linux file system may require some explanation
to understand.

The Linux file system is explained well by this short video,
['Linux Directories Explained in 100 Seconds'][4] by the Youtube channel
Fireship.

## Windows / Linux, Filepath Differences

There are a few differences between Windows and Linux filepaths.

Windows Filepaths:

 * Are case insensitive.
 * Use a backslash to separate folders.
 * Begin with a drive letter.
 * User files stored in `C:\Users\[username]``

```
C:\Users\bob\folder\file.txt
```

Linux Filepaths:

 * Are case sensitive.
 * Use a forward slash to separate folders.
 * Begin with a forward slash.
 * User files stored in `/home/[username]``

 ```
 /home/bob/folder/file.txt
 ```


[1]: https://www.virtualbox.org/
[2]: https://system76.com/
[3]: https://www.howtogeek.com/140679/beginner-geek-how-to-start-using-the-linux-terminal/
[4]: https://www.youtube.com/watch?v=42iQKuQodW4
