---
title: "Testing for Linux"
category: "General Advice"
order: 3
---

## Test Machine Setup

There are two approaches to setting up a testing machine for both Windows and
Linux. The first option is to dual boot your testing machine, installing both
Windows and Linux alongside each other. Changing OS then requires a simple
reboot to switch to the other OS.

Alternatively, you can perform all your development on one OS, and use a virtual
machine to test the other. For your guest OS to have access to your host OS's GPU,
you will need to setup your guest OS VM with 'GPU Passthrough'.

Please note, this comes with the warning that GPU passthrough is not always
reliable, can be difficult to setup, can set off some anticheat middleware, and
doesn't always replicate graphics driver behaviour.

## Linux Distributions To Test

It is safe to simply test your game against the latest Ubuntu LTS version
(currently Ubuntu 20.04).

With that said, however, you may want to consider testing other popular
distributions as well.
[You can see the currently most popular distributions according to Steam here](https://store.steampowered.com/hwsurvey?platform=linux).

## Test Multiple GPUs

Much like on Windows, wildly different results for the same games can occur from
difficult GPUs. As a minimum, it is recommended to test at least AMD + Mesa,
NVIDIA and Intel + Mesa.

There is, for example, strong circumstantial evidence that NVIDIA GPUs are far
more fault tolerant of bad OpenGL code than other vendors.

## Game Engines That Support Linux Still Require Testing

Game engines like Unreal Engine and Unity are incredibly helpful. These engines
offer you help for exporting your game to Linux, taking care of the building
and/or packaging process. This greatly reduces the effort needed to get your
game running on Linux, however, it is still necessary to test your game running
on a Linux development machine as you would do on Windows, as there  sometimes
can be Linux-specific problems.
