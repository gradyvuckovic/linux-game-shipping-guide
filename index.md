---
title: Introduction
---

This guide is for any game developer evaluating bringing their current or next
PC game to Linux, who has concerns about the difficulty, practicality, financial
return of doing so, or looking for guidance on the best practices to follow.

Information about the Linux platform, advice on how to reduce the amount of work
involved, warnings for common potential pitfalls and advice on how to best
market your title is provided within.
